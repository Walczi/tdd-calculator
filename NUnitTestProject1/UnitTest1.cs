﻿using System;
using NUnit.Framework;
using SE2_1;

namespace UnitTestProject1
{
    public class UnitTest1
    {
        [Test]
        public void EmptyStringReturnsZeroTest()
        {
            Calculator calc = new Calculator();
            Assert.AreEqual(calc.Add(""), 0);
        }

        [Test]
        public void ReturnNumberReturnsValueTest()
        {
            Calculator calc = new Calculator();
            Assert.AreEqual(calc.Add("10"), 10);
            Assert.AreEqual(calc.Add("1"), 1);
            Assert.AreEqual(calc.Add("0"), 0);
            Assert.AreEqual(calc.Add("12"), 12);
        }

        [Test]
        public void TwoNumbersWithCommaReturnsSumTest()
        {
            Calculator calc = new Calculator();
            Assert.AreEqual(calc.Add("10,2"), 12);
            Assert.AreEqual(calc.Add("1,5"), 6);
            Assert.AreEqual(calc.Add("0,13"), 13);
            Assert.AreEqual(calc.Add("12,2"), 14);
        }

        [Test]
        public void TwoNumbersWithNewLineReturnsSumTest()
        {
            Calculator calc = new Calculator();
            Assert.AreEqual(calc.Add("10\n2"), 12);
            Assert.AreEqual(calc.Add("1\n5"), 6);
            Assert.AreEqual(calc.Add("0\n13"), 13);
            Assert.AreEqual(calc.Add("12\n2"), 14);
        }

        [Test]
        public void ManyNumbersWithDelimitedReturnsSumTest()
        {
            Calculator calc = new Calculator();
            Assert.AreEqual(calc.Add("10\n2,3,5"), 20);
            Assert.AreEqual(calc.Add("1\n5\n6"), 12);
            Assert.AreEqual(calc.Add("0,13,45\n7"), 65);
            Assert.AreEqual(calc.Add("12,2\n10"), 24);
        }

        //[Test]
        //[ExpectedException(typeof(Exception))]
        //public void NegativeNumbersThrowsExceptionTest()
        //{
        //    Calculator calc = new Calculator();
        //    calc.Add("-412");
        //}

        [Test]
        public void NumberGreaterThen1000AreIgnoratedTest()
        {
            Calculator calc = new Calculator();
            Assert.AreEqual(calc.Add("1000"), 0);
            Assert.AreEqual(calc.Add("1,3,111000"), 4);
            Assert.AreEqual(calc.Add("0,5,20000"), 5);
            Assert.AreEqual(calc.Add("999,1\n1"), 1001);
        }

        [Test]
        public void SingleCharDelimiterDefinedAtTheBeginingTest()
        {
            Calculator calc = new Calculator();
            Assert.AreEqual(calc.Add("//#10#2#3#5"), 20);
            Assert.AreEqual(calc.Add("//?1?5?6"), 12);
            Assert.AreEqual(calc.Add("//&0&13&45&7"), 65);
            Assert.AreEqual(calc.Add("//*12*2*10"), 24);
        }
    }
}
