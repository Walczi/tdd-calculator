﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SE2_1
{
    public class Calculator
    {
        public int Add(String str)
        {
            int result = 0;
            if (str.Length > 0)
            {
                List<char> delimiters = new List<char>();
                delimiters.Add(',');
                delimiters.Add('\n');       
                if (str.Length > 3 && str.Substring(0,2) == "//")
                {
                    delimiters.Add(str[2]);
                    str = str.Remove(0, 3);
                }
                string[] words = str.Split(delimiters.ToArray());
                foreach (var w in words)
                {
                    int value = Int32.Parse(w);
                    if (value < 0)
                        throw new Exception();
                    if(value < 1000)
                        result += value;
                }
            }
            return result;
        }
    }
}
